class RPNCalculator
  attr_accessor :stack

  def initialize
    @stack = []
  end

  def plus
    num2 = pop
    num1 = pop
    @stack << num1 + num2
  end

  def minus
    num2 = pop
    num1 = pop
    @stack << num1 - num2
  end

  def divide
    num2 = pop
    num1 = pop
    @stack << num1 / num2
  end

  def times
    num2 = pop
    num1 = pop
    @stack << num1 * num2
  end

  def push(num)
    @stack << num.to_f
  end

  def value
    @stack[-1]
  end

  def pop
    pop = @stack.pop
    pop.nil? ? (raise 'calculator is empty') : pop
  end

  def tokens(str)
    list = []
    ops = %w[+ - * /]
    str.split(" ").each do |char|
      ops.include?(char) ? list << char.to_sym : list << char.to_i
    end
    list
  end

  def evaluate(str)
    list = tokens(str)
    list.each do |char|
      case char
      when :+
        plus
      when :-
        minus
      when :/
        divide
      when :*
        times
      else
        push(char)
      end
    end
    value
  end
end
